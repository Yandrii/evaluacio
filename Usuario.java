package com.example.xino.conserje;

public class Usuario {
    private Integer id;
    private String nombre;
    private String cedula;
    private String  apellidos;
    private String  fecha;
    private String  labora;


    public Usuario(Integer id, String nombre, String cedula, String apellidos, String fecha, String labora) {
        this.id = id;
        this.nombre = nombre;
        this.cedula = cedula;
        this.apellidos = apellidos;
        this.fecha = fecha;
        this.labora = labora;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getLabora() {
        return labora;
    }

    public void setLabora(String labora) {
        this.labora = labora;
    }
}

